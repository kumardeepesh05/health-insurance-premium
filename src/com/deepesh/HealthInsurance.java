package com.deepesh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HealthInsurance {
	
	public static void main (String args[]) throws IOException{
		
		String name;
		String gender;
		int age;
		String curr_health_hypertension, curr_health_bloodPressure, curr_health_blood_suger, curr_health_overweight;
		String habit_smoking, habit_alcohol, habit_daily_exer, habit_drugs;
		int cnt_pre_condition=0, cnt_habit=0, hip_amt_int;
		double hip_amt = 5000;
		
			
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		//System.out.print("Name: ");
		name = br.readLine();
		//System.out.print("Gender: ");
		gender = br.readLine();
		//System.out.print("Age (Years): ");
		age = Integer.parseInt(br.readLine());
		//System.out.println("Current health:");
		//System.out.print("\tHypertension: ");
		curr_health_hypertension = br.readLine();
		//System.out.print("\tBlood pressure: ");
		curr_health_bloodPressure = br.readLine();
		//System.out.print("\tBlood sugar: ");
		curr_health_blood_suger = br.readLine();
		//System.out.print("\tOverweight: ");
		curr_health_overweight = br.readLine();
		//System.out.println("Habits: ");
		//System.out.print("\tSmoking:");
		habit_smoking = br.readLine();
		//System.out.print("\tAlcohol: ");
		habit_alcohol = br.readLine();
		//System.out.print("\tDaily exercise: ");
		habit_daily_exer = br.readLine();
		//System.out.print("\tDrugs: ");
		habit_drugs = br.readLine();
		
		if(age>=18){
			hip_amt += (hip_amt*10/100);
			if(age>=25){
				hip_amt += (hip_amt*10/100);
				if(age>=30){
					hip_amt += (hip_amt*10/100);
					if(age>=35){
						hip_amt += (hip_amt*10/100);
						while(age>=40){
							hip_amt += (hip_amt*20/100);
							age -= 5;
						}
					}
				}
			}
		}
		
		if(gender.equalsIgnoreCase("male")){
			hip_amt += (hip_amt*2/100);
		}
		
		if(curr_health_hypertension.equalsIgnoreCase("yes")){
			cnt_pre_condition++;
		}
		if(curr_health_bloodPressure.equalsIgnoreCase("yes")){
			cnt_pre_condition++;
		}
		if(curr_health_blood_suger.equalsIgnoreCase("yes")){
			cnt_pre_condition++;
		}
		if(curr_health_overweight.equalsIgnoreCase("yes")){
			cnt_pre_condition++;
		}
		
		hip_amt += (hip_amt*(1*cnt_pre_condition)/100);
		
		
		if(habit_smoking.equalsIgnoreCase("yes")){
			cnt_habit++;
		}
		if(habit_alcohol.equalsIgnoreCase("yes")){
			cnt_habit++;
		}
		if(habit_daily_exer.equalsIgnoreCase("yes")){
			cnt_habit--;
		}
		if(habit_drugs.equalsIgnoreCase("yes")){
			cnt_habit++;
		}
		hip_amt += (hip_amt*(3*cnt_habit)/100);
		
		hip_amt_int = (int) (hip_amt + .50);
		
		System.out.println("Health Insurance Premium for " + name + ": Rs. " + hip_amt_int);
	}	
}
